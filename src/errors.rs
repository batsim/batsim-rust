//! When something goes wrong.
use crate::BatsimMessage;
use thiserror::Error;

///////////////////////////////////////////////////////////

#[derive(Error, Debug)]
/// This type represents all possible errors that can occur when using batsim-rust's API.
pub enum Error {
    /// Scheduler encountered an error --- e.g., it received an unhandled event type
    #[error("scheduler: {0}")]
    Scheduler(String),

    /// The simulation is finished.
    /// This is returned by schedulers when the [SIMULATION_ENDS](../protocol/enum.BatsimEvent.html#variant.SIMULATION_ENDS) event is received.
    #[error("simulation is finished (according to batsim)")]
    SimulationFinished(),

    /// Any network-related error
    #[error("network (zmq): {0}")]
    Zmq(zmq::Error),

    /// A non-UTF8 message has been read from Batsim
    #[error("non-UTF8 message received")]
    BadlyEncodedMessage(),

    /// Any serialization error
    #[error("cannot serialize: {0} --- source input was `{1:?}`")]
    Serialization(serde_json::Error, BatsimMessage),

    /// Any deserialization error
    #[error("cannot deserialize: {0} --- source input was `{1}`")]
    Deserialization(serde_json::Error, String),

    /// Another error occured (i.e., one not explicitly defined in previous variants)
    #[error("{0}")]
    Other(String),
}
