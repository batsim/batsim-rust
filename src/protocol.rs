//! Types related to the Batsim protocol.
//!
//! Please refer to [Batsim's official protocol documentation](https://batsim.readthedocs.io/en/latest/protocol.html) for more information.
use std::collections::HashMap;
use std::collections::BTreeMap as Map;
use serde::{Serialize, Deserialize};
use serde_json::Value;
use serde_json::Result;

#[derive(Debug, Serialize, Deserialize)]
pub struct Job {
    pub id: String,
    pub res: i32,
    pub profile: String,
    pub subtime: f64,
    pub walltime: Option<f64>,
    #[serde(flatten)]
    pub other: Map<String, Value>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(tag = "type")]
// https://batsim.readthedocs.io/en/latest/input-workload.html#profile-types-overview
pub enum Profile {
    #[serde(rename = "delay")]
    Delay { delay: f64 },
    #[serde(rename = "parallel_homogeneous")]
    ParallelHomogeneous { com: f64, cpu: f64 },
    #[serde(rename = "parallel_homogeneous_total")]
    ParallelHomogeneousTotal { com: f64, cpu: f64 },
    // TODO: support more profile types
    // TODO: keep additional fields entered by users
}

#[derive(Debug, Serialize, Deserialize)]
pub struct BatsimMessage {
    pub now: f64,
    pub events: Vec<BatsimEvent>,
}

impl BatsimMessage {
    pub fn new(now: f64) -> BatsimMessage {
        BatsimMessage { now: now, events: vec![] }
    }

    pub fn from_str(s: &String) -> Result<BatsimMessage> {
        serde_json::from_str(&s)
    }

    pub fn to_string(&self) -> Result<String> {
        serde_json::to_string(&self)
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RegisterJob {
    pub job_id: String,
    pub job: Job,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RegisterProfile {
    pub job_id: String,
    pub job: Job,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Notify {
    #[serde(rename = "type")]
    pub notify_type: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct JobSubmitted {
    pub job_id: String,
    pub job: Job,
    pub profile: Option<Profile>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RejectJob {
    pub job_id: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ExecuteJob {
    pub job_id: String,
    pub alloc: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub mapping: Option<HashMap<String, String>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct KillJob {
    pub job_ids: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct JobKilled {
    pub job_ids: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct JobCompleted {
    pub job_id: String,
    pub job_state: String,
    pub return_code: i32,
    pub alloc: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SimulationBegins {
    pub config: serde_json::Value,
    pub nb_resources: i32,
}

#[allow(non_camel_case_types)]
#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum BatsimEvent {
    /// From Batsim to the scheduler
    SIMULATION_BEGINS {
        timestamp: f64,
        data: SimulationBegins,
    },
    SIMULATION_ENDS {
        timestamp: f64,
    },
    JOB_SUBMITTED {
        timestamp: f64,
        data: JobSubmitted,
    },
    JOB_COMPLETED {
        timestamp: f64,
        data: JobCompleted,
    },
    JOB_KILLED {
        timestamp: f64,
        data: JobKilled,
    },
    // TODO: RESOURCE_STATE_CHANGED
    // TODO: REQUESTED_CALL

    /// From the scheduler to Batsim
    REJECT_JOB {
        timestamp: f64,
        data: RejectJob,
    },
    EXECUTE_JOB {
        timestamp: f64,
        data: ExecuteJob,
    },
    // TODO: CALL_ME_LATER
    KILL_JOB {
        timestamp: f64,
        data: KillJob,
    },
    REGISTER_JOB {
        timestamp: f64,
        data: RegisterJob,
    },
    REGISTER_PROFILE {
        timestamp: f64,
        data: RegisterProfile,
    },
    // TODO: SET_JOB_METADATA
    // TODO: TODO CHANGE_JOB_STATE

    /// Bidirectional
    // TODO: QUERY
    // TODO: ANSWER
    NOTIFY {
        timestamp: f64,
        data: Notify,
    },
}
