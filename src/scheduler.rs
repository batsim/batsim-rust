//! Generic interface for schedulers.
use crate::{BatsimMessage, Error};

/// Defines a scheduler behavior and interface.
pub trait Scheduler {
    /// Called when the first message is received from Batsim.
    ///
    /// Default implementation does nothing (simulation time is unchanged, no decision is taken).
    /// **Hint**: You probably want to override this function to initialize data structures related to available resources.
    fn init(&mut self, msg: &BatsimMessage) -> Result<BatsimMessage, Error> {
        Ok(BatsimMessage::new(msg.now))
    }

    /// Called when a message is received from Batsim (unless this is the first message, in which case [init](#method.init) is called instead).
    ///
    /// **Important**: Scheduler implementations **MUST** return the following types. Example implementation below is a valid example.
    /// - `Ok(None)` if and only if a [SIMULATION_ENDS](../protocol/enum.BatsimEvent.html#variant.SIMULATION_ENDS) event has been received.
    /// - `Err(Error::Scheduler)` should be returned when an unrecoverable error is encountered (cf. [Error::Scheduler](../errors/enum.Error.html#variant.Scheduler)).
    /// - Otherwise, `Ok(Some(...))` should be returned, even if no decision is taken. In this case, the BatsimMessage should contain 0 events.
    ///
    /// ```
    /// fn take_decisions(&mut self, msg: &BatsimMessage) -> Result<Option<BatsimMessage>, Error> {
    ///     for event in &msg.events {
    ///         match event {
    ///             BatsimEvent::SIMULATION_ENDS {timestamp: _} => return Ok(None),
    ///             x => return Err(Error::Scheduler(format!("Unhandled event received: {:?}", x))),
    ///         }
    ///     }
    ///     Ok(Some(BatsimMessage::new(msg.now)))
    /// }
    /// ```
    fn take_decisions(&mut self, msg: &BatsimMessage) -> Result<Option<BatsimMessage>, Error>;
}
