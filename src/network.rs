//! To communicate with Batsim over network.
use zmq;

use crate::{Error, Scheduler, BatsimMessage, BatsimEvent};

pub struct NetworkContext {
    #[allow(dead_code)]
    zmq_context: zmq::Context,
    zmq_socket: zmq::Socket,
}

impl NetworkContext {
    pub fn new(endpoint: &str) -> Result<Self, Error> {
        let context = zmq::Context::new();
        let socket = match context.socket(zmq::REP) {
            Ok(x) => x,
            Err(x) => return Err(Error::Zmq(x))
        };

        match socket.bind(endpoint) {
            Ok(_) => Ok( Self {
                zmq_context: context,
                zmq_socket: socket,
            }),
            Err(x) => Err(Error::Zmq(x))
        }
    }

    pub fn send_msg(&mut self, msg: BatsimMessage) -> Result<(), Error> {
        let msg = match serde_json::to_string(&msg) {
            Ok(x) => x,
            Err(x) => return Err(Error::Serialization(x, msg)),
        };

        match self.zmq_socket.send(&msg, 0) {
            Ok(_) => Ok(()),
            Err(x) => Err(Error::Zmq(x)),
        }
    }

    pub fn recv_msg(&mut self) -> Result<BatsimMessage, Error> {
        let msg = match self.zmq_socket.recv_msg(0) {
            Ok(x) => x,
            Err(err) => return Err(Error::Zmq(err)),
        };

        let msg = match msg.as_str() {
            Some(x) => x,
            None => return Err(Error::BadlyEncodedMessage()),
        };

        match serde_json::from_str(msg) {
            Ok(x) => Ok(x),
            Err(x) => Err(Error::Deserialization(x, msg.to_string())),
        }
    }
}

pub fn run_whole_simulation(sched: &mut Box<dyn Scheduler>, context: &mut NetworkContext) -> Result<(), Error> {
    let first_msg = context.recv_msg()?;
    if first_msg.events.len() != 1 {
        return Err(Error::Other(format!("first received message has {} events while 1 was expected (SIMULATION_BEGINS): `{:?}`", first_msg.events.len(), first_msg)));
    }
    let decisions = match &first_msg.events[0] {
        BatsimEvent::SIMULATION_BEGINS {timestamp: _, data: _} => sched.init(&first_msg)?,
        x => return Err(Error::Other(format!("first received event in first message is not SIMULATION_BEGINS: `{:?}`", x))),
    };
    context.send_msg(decisions)?;

    loop {
        match context.recv_msg() {
            Err(x) => return Err(x),
            Ok(msg) => {
                let decisions = match sched.take_decisions(&msg) {
                    Ok(Some(x)) => x,
                    Ok(None) => {
                        context.send_msg(BatsimMessage::new(msg.now))?;
                        return Ok(());
                    },
                    Err(x) => return Err(x),
                };
                context.send_msg(decisions)?;
            }
        }
    }
}
