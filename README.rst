batsim-rust
===========

batsim-rust is a Rust library to help the design of Batsim_ schedulers.

- Provides a ``Scheduler`` traits_ onto which you can build your schedulers.
- Provides structs_ for the `Batsim protocol`_ shenanigans (de/serialization via serde_).
- Provides convenience functions_ to reshape protocol events — e.g., to group them by type.
- Provides functions_ to support network request-reply (via ZeroMQ_) and simulation loop helpers.

Usage
-----

TODO: cargo dependency line

TODO: trivial scheduler with the provided API

For a more complete usage example, give a look at oxidisched_.

Rationale
---------

- Straightforward and generic API, so it is easy to use and understand.
- Propagate errors instead of panicking, so the caller code can manage errors how it wants.
- Provide convenience function so that schedulers are as concise as possible.
- Separate the serialization and the network part of the protocol. This enables Batsim to ask for scheduling decisions via a library function call rather than a network message exchange with another process, which has less overhead.

Inspirations
----------------

- My previous work on batsched_.
- Adrien Faure's work on `batsim.rs`_.

.. _Batsim: https://batsim.rtfd.io
.. _Batsim protocol: https://batsim.readthedocs.io/en/latest/protocol.html

.. _serde: https://github.com/serde-rs/serde
.. _ZeroMQ: https://zeromq.org/
.. _batsched: https://framagit.org/batsim/batsched
.. _batsim.rs: https://github.com/adfaure/batsim.rs
.. _oxidisched: https://gitlab.inria.fr/batsim/oxidisched

.. _traits: https://doc.rust-lang.org/1.8.0/book/traits.html
.. _structs: https://doc.rust-lang.org/1.8.0/book/structs.html
.. _functions: https://doc.rust-lang.org/1.8.0/book/functions.html
